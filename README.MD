
#File system manager

A program where a user can choose different options for listing and get information about files.

### Main menu 
The main menu consists of different options to show files in the directory /resources.

__List filenames:__ This option will list the name of each file in the directory

__List filenames with specific extension:__ Will list all available extensions and based on the choice from user lists 
the filenames of the given extension. 

__Dracula menu:__ This option will show the submenu.

__Quit program:__ Wil terminate the program. 

###Sub menu
This menu consists of options to get information about and read a given text file.

__Get filename:__ This option will return the name of the given file to the user.

__Get filesize:__ This will show the size of the given file in kB.

__Get number of lines in file:__ This choice will return the number of lines in the given file.

__Search for word:__ Takes a word from the user and look for it in the file. 

__Number of occurrences of word:__ Count the number of times a word occurs in the file. The actual word has to be the 
same fex: if the word is "friend", the word "friends" will not be counted. 

__Return to main menu__ This will take the user back to the main menu. 
### Logging
All results of choices from the user are logged with a timestamp and duration of execution. 
