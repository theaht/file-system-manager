package main.filemanipulation;

import java.io.File;
import java.util.ArrayList;


public class FileListService {


    /**
     * Method retrieves the filenames from the directory resources,
     * from the file-method listFiles().
     * @return String array with the name of the files.
     */
    public String [] getFileNames() {
        File folder;
        String [] filenames;
        File[] files;

        try{
            folder = new File("resources");
            files = folder.listFiles();
            if(files != null){
                filenames = new String[files.length];

                for (int i = 0; i < files.length; i++) {
                    filenames[i] = files[i].getName();
                }
                return filenames;
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

        return null;
    }

    /**
     * This method calls the method getFileNames.
     * Loops through the filenames and stores all available file extensions in an arraylist.
     * @return list of available extensions.
     */
    public ArrayList<String> getFileExtensions() {
        String [] filenames = getFileNames();
        ArrayList<String> extensions = new ArrayList<>();

        if(filenames == null) {
            return null;
        }

        for (String filename : filenames) {
            String extension = filename.substring(filename.lastIndexOf("."));
            if (!extensions.contains(extension.toLowerCase())) {
                extensions.add(extension);
            }
        }

        return extensions;
    }

    /**
     * Gets the list of files from getFileName(), and based on the parameter extension,
     * it sorts out the files that fits the condition.
     * @param extension extension to search for in list
     * @return list of files with specified extension.
     * */
    public ArrayList<String> getExtensionSpecificFilenames(String extension){
        String [] filenames = getFileNames();
        ArrayList<String> specificFiles = new ArrayList<>();

        if(filenames == null){
            return null;
        }

        for(String filename : filenames){
            if(filename.substring(filename.lastIndexOf(".")).compareToIgnoreCase(extension) == 0) {
                specificFiles.add(filename);
            }
        }
        return specificFiles;

    }



}
