package main.filemanipulation;

import java.io.*;


public class TxtFileService {

    File file;

    /**
     * Constructor creates a File object from the filename provided as parameter.
     * @param filename the name/path of the file.
     */
    public TxtFileService (String filename) {
        this.file = new File(filename);
    }
    /**
     * Method returns the name of the file, extension included.
     * @return filename
     */
    public String getFileName() {
        return this.file.getName();
    }

    /**
     * Method returns the size of file in kB.
     * @return filesize
     */
    public long getFileSizeKB() {
        return this.file.length()/1024;
    }

    /**
     * Reads file. If wanted word is found, the method stops reading and returns true.
     * If wanted word is not found, returns false.
     * @param wantedWord the word to search for
     * @return if the word is found or not.
     */
    public boolean wordExists(String wantedWord) {

        BufferedReader reader = null;
        try {

            reader = new BufferedReader(new FileReader(this.file));
            String line;

            while((line =reader.readLine()) != null){
                line = line.replaceAll("[^\\w\\s]", " ");
                String [] lineAsArray = line.split(" ");
                for(String word : lineAsArray){
                    if(word.compareToIgnoreCase(wantedWord) == 0){
                        return true;

                    }
                }
            }
        }
        catch(IOException e) {
            System.out.println(e.getMessage());
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return false;


    }

    /**
     * Finds the number of lines in file.
     * @return the number of lines
     */
    public long numberOfLines(){
        long count = 0;
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(this.file));
            count = reader.lines().count();


        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return count;
    }

    /**
     * Count occurrences of a word in file. Regex is used to include occurrences where
     * the word has special characters (for example if the word is at the end of the sentence, and a . is included)
     * @param wantedWord the word to count occurrences of
     * @return the number of occurrences. 0 if word does not occur.
     * */
    public int countOccurrencesOfWord(String wantedWord) {
        int count = 0;
        String line;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(this.file));

            while((line =reader.readLine()) != null){
               line = line.replaceAll("[^\\w\\s]", " ");
               String [] lineAsArray = line.split(" ");
               for(String word : lineAsArray){
                   if(word.compareToIgnoreCase(wantedWord) == 0){
                       count++;
                   }
               }
            }

        }
        catch(IOException e) {
            System.out.println(e.getMessage());
        }
        finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return count;
    }


}
