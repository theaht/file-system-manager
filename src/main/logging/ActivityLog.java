package main.logging;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityLog {
    String filename;

    /**
     * Constructor sets the name of the file to write to.
     * @param filename name of file
     */
    public ActivityLog(String filename) {

        this.filename = filename;
    }


    /**
     * method to write an activity to file. Finds the current timestamp and
     * concatenates it with the text provided, and writes it to file. If file does not
     * exists, new file is created. If file exists, append to existing file
     * @param text text to write to file.
     */
    public void addActivity(String text) {
        String currentTime = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
        String log = currentTime + " " + text + "\n";
        FileWriter writer = null;
        try{
            writer = new FileWriter(this.filename, true);
            writer.write(log);
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        finally {
            if(writer != null){
                try{
                    writer.close();
                }
                catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

    }


}
