package main;

import main.filemanipulation.FileListService;
import main.filemanipulation.TxtFileService;
import main.logging.ActivityLog;


import java.util.ArrayList;
import java.util.Scanner;

public class FileSystemManager {

    Scanner sc = new Scanner(System.in);

    /**
     * Main method. While loop that calls the method mainMenu(), when mainMethod returns false,
     * the while loop ends and the program terminates.
     */
    public static void main(String [] args){
        FileSystemManager f = new FileSystemManager();
        FileListService fls = new FileListService();
        TxtFileService tfs = new TxtFileService("resources/Dracula.txt");
        ActivityLog aLog = new ActivityLog("logs/activityLog.txt");


        boolean cont = true;
        while(cont) {
            cont = f.mainMenu(fls, tfs, aLog);
        }
    }

    /**
     * Method that shows the different actions to the user. And based on the user's choice,
     * the right method is called in a switch.
     * This method also time the executions of the methods being called.
     * The result and duration is sent to the method addActivity, which writes to the log.
     * If the user types in an invalid choice the user is asked to type in a valid choice.
     * @return a boolean, if true this method will be called again, if false the program terminate.
     */
    public boolean mainMenu(FileListService fls, TxtFileService tfs, ActivityLog aLog){
        boolean cont = true;

        System.out.println("1. List filenames");
        System.out.println("2. List filenames with specific extension");
        System.out.println("3. Dracula menu");
        System.out.println("4. Quit program");

        int choice = getUserChoice();

        long startTime;
        long endTime;
        long duration;
        String list;

        switch (choice) {
            case 1 -> {
                startTime = System.currentTimeMillis();
                list = printFilenames(fls);
                System.out.println(list);
                endTime = System.currentTimeMillis();
                duration = endTime - startTime;
                aLog.addActivity("All files were listed. The function took " +
                        duration + "ms to execute, Returned:\n" + list);
            }
            case 2 -> {
                System.out.println("Choose the type of files you want: ");
                startTime = System.currentTimeMillis();
                String wantedWord = getUserExtension(fls);
                list = specificFiles(fls, wantedWord);
                System.out.println(list);
                endTime = System.currentTimeMillis();
                duration = endTime - startTime;
                aLog.addActivity("Files with specific extension were listed. The function took " +
                        duration + "ms to execute, Returned:\n" + list);
            }
            case 3 -> subMenu(tfs, aLog);
            case 4 -> {
                sc.close();
                cont = false;
            }
            default -> System.out.println("Please type in a valid choice");
        }
        return cont;
    }

    /**
     * This method shows all choices for the text file. Based on user input,
     * the right method is called and executed.
     * Execution of the methods are timed. Result and duration is sent to the method addActivity.
     */
    public void subMenu(TxtFileService tfs, ActivityLog aLog) {
        boolean cont = true;

        while(cont){
            System.out.println("1. Get filename");
            System.out.println("2. Get filesize");
            System.out.println("3. Get number of lines in file");
            System.out.println("4. Search for word");
            System.out.println("5. Number of occurrences of word");
            System.out.println("6. Return to main menu");

            int choice = getUserChoice();
            long startTime;
            long endTime;
            long duration;

            switch (choice) {
                case 1 -> {
                    startTime = System.currentTimeMillis();
                    String filename = tfs.getFileName();
                    System.out.println("Filename: " + filename);
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    aLog.addActivity("Get filename. Executed in " + duration + "ms, Returned: " + filename);
                }
                case 2 -> {
                    startTime = System.currentTimeMillis();
                    long filesize = tfs.getFileSizeKB();
                    System.out.println("Filesize: " + filesize + "kB");
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    aLog.addActivity("Get filesize. Executed in " +
                            duration + "ms, Returned: " + filesize + "kB");
                }
                case 3 -> {
                    startTime = System.currentTimeMillis();
                    long lines = tfs.numberOfLines();
                    System.out.println("Number of lines: " + lines + " lines");
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    aLog.addActivity("Get number of lines. Executed in " +
                            duration + "ms, Returned: " + lines);
                }
                case 4 -> {
                    String word = getUserWord();
                    startTime = System.currentTimeMillis();
                    boolean exists = tfs.wordExists(word);
                    System.out.println(word + " exists in file: " + exists);
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    aLog.addActivity("Word: " + word + " exists in file? Executed in " +
                            duration + "ms, Returned: " + exists);
                }
                case 5 -> {
                    String word1 = getUserWord();
                    startTime = System.currentTimeMillis();
                    int count = tfs.countOccurrencesOfWord(word1);
                    System.out.println("Number of occurrences of word " + word1 + ": " + count);
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    aLog.addActivity("Number of occurrences of word: " +
                            word1 + "  Executed in " + duration + "ms, Returned: " + count);
                }
                case 6 -> cont = false;
                default -> System.out.println("Please type in a valid choice");
            }
        }
    }

    /**
     * This method takes a number from the user.
     * @return choice the number received from the user.
     */
    public int getUserChoice(){
        int choice;
        System.out.print("Choice: ");
        while(!sc.hasNextInt()) {
            System.out.println("Please enter a number that is a valid choice");
            sc.next();
        }

        choice = sc.nextInt();

        return choice;
    }

    /**
     * Method receives a word from the user.
     * @return word the word received from the user
     * */
    public String getUserWord(){
        System.out.print("Word: ");
        return sc.next();
    }

    /**
     * This methods ask for a extension from the user.
     * If the extension does not match one of the available extensions,
     * the user will be asked again.
     * @return wantedExtension the extension received by the user.
     * */
    public String getUserExtension(FileListService fls) {
        ArrayList<String> extensions = fls.getFileExtensions();

        for (String extension : extensions) {
            System.out.println(extension);
        }

        String wantedExtension = sc.next();
        while(!extensions.contains(wantedExtension.toLowerCase())){
            System.out.print("Please type in one of the available extensions");
            wantedExtension = sc.next();

        }

        return wantedExtension;
    }

    /**
     * Calls the method getFileNames(), and builds a string from the list of filenames received.
     * @return a string with all the filenames.
     */
    public String printFilenames(FileListService fls){
        String [] filenames = fls.getFileNames();
        StringBuilder list = new StringBuilder();
        if(filenames == null) {
            list = new StringBuilder("There are no files in the directory");
        }
        else {
            for (String filename : filenames) {
                list.append("\t").append(filename).append("\n");
            }
        }
        return list.toString();
    }

    /**
     * Calls the method getExtensionSpecificFilenames(), and builds a string from the list of filenames received.
     * @return a string with all the filenames.
     */
    public String specificFiles(FileListService fls, String extension){

        StringBuilder list = new StringBuilder();
        ArrayList<String> specificFiles = fls.getExtensionSpecificFilenames(extension);

        for(String filename : specificFiles){
            list.append("\t").append(filename).append("\n");
        }

        return list.toString();
    }
}
